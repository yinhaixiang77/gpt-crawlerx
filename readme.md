# GPT CrawlerX

爬取网站以生成知识文件，从一个或多个 URL 创建您自己的定制 GPT

> 项目本身是参考[gpt-crawler](https://github.com/BuilderIO/gpt-crawler)
> 只是因为我需要爬取的几个网站gpt-crawler都不支持, 所以就自己写了个gpt-crawlerx, 因此使用方法和gpt-crawler都一样, 可以参考gpt-crawler的readme.

## 特性
- 使用 Playwright 自动化浏览网页。
- 提取网页的标题和指定选择器的内容。
- 支持页面访问重试机制，增强爬虫的鲁棒性。
- 将爬取结果保存为时间戳命名的 JSON 文件。

## 安装
首先，确保您的系统已经安装了 Node.js。然后，您可以克隆此仓库并安装所需依赖：
  ```
  git clone https://gitee.com/yinhaixiang77/gpt-crawlerx
  cd [项目目录]
  npm install
  ```

## 使用
1. **配置**：在项目根目录下创建一个 `option.json` 文件，包含以下内容：
  ```
  {
    "url": "要爬取的起始网页 URL",
    "match": "要匹配的链接的通配符",
    "selector": "要提取内容的 CSS 选择器",
    "maxPagesToCrawl": "最大爬取页面数"
  }
  ```

2. **运行爬虫**：在项目根目录下运行以下命令以启动爬虫：

  ```
  npm start
  ```

3. **查看结果**：爬虫运行完成后，会在 `target` 目录下生成一个以output-${yyyyMMddhhmmss}命名的包含爬取结果的 JSON 文件。

## 注意事项
- 确保您爬取的网站允许被爬取，遵守 `robots.txt` 文件和相关法律法规。
- 本项目仅用于学习和研究目的，使用时请尊重网站所有者的权益。

### 将您的数据上传到 OpenAI
爬行将在此项目的根目录生成名为 `output.json` 的文件。将其上传 [到 OpenAI](https://platform.openai.com/docs/assistants/overview) 以创建您的自定义助手或自定义 GPT。

#### 创建自定义 GPT
如果您需要 UI 访问您生成的知识，并且可以轻松地与他人共享，请使用此选项

> 注意：您可能需要付费的 ChatGPT 计划才能创建和使用自定义 GPT

1. 访问 [https://chat.openai.com/](https://chat.openai.com/)
2. 点击左下角的您的姓名
3. 在菜单中选择 "我的 GPT"
4. 选择 "创建 GPT"
5. 选择 "配置"
6. 在 "知识" 下选择 "上传文件" 并上传您生成的文件
7. 如果您收到文件太大的错误，您可以尝试将其拆分为多个文件并分别上传，使用 config.ts 文件中的 maxFileSize 选项，或者也可以使用 config.ts 文件中的 maxTokens 选项对文件进行标记化以减小文件大小

![如何上传自定义 GPT 的 GIF](https://taosha01-1253585015.cos.ap-shanghai.myqcloud.com/typora/aa.gif)


#### 创建自定义助手
如果您需要 API 访问您生成的知识，并将其集成到您的产品中，请使用此选项。

1. 访问 [https://platform.openai.com/assistants](https://platform.openai.com/assistants)
2. 点击 "+ 创建"
3. 选择 "上传" 并上传您生成的文件

![如何上传到助手的 GIF](https://taosha01-1253585015.cos.ap-shanghai.myqcloud.com/typora/bb.gif)


### 许可
该项目根据 MIT 许可证授权。有关详细信息，请参阅 [LICENSE](LICENSE) 文件。

## 其他
我开发的其他一些微不足道的项目:
- https://taosha.club/    资讯/播客/搜索聚合平台
- http://t.taosha.club/    纯净版搜索聚合平台(百度+谷歌)
- http://ai.taosha.club/    利用gpt+搜索聚合能力实现AI搜索
- http://chat.taosha.club/    chatgpt套壳网站

## 贡献
欢迎通过 Pull Requests 或 Issues 对项目进行贡献和提出改进建议。